Rails.application.routes.draw do

  devise_for :admins
  namespace :admins do
    root "transactions#index"
    resources :transactions
  end

  concern :api_routes do
    resources :transactions, only: [:index] do
      collection do
        post :buy
        post :withdraw
        post :sell
        post :top_up
      end
    end
    resources :balances, only: [:index]
  end

  scope module: 'api', path: 'api', defaults: { format: 'json' } do
    devise_for :users, controllers: { sessions: :sessions, registrations: :registrations }
    devise_scope :user do
      post 'register', to: 'registrations#create'
      post 'login', to: 'sessions#create'
    end
    concerns :api_routes
  end
end
