require "rails_helper"

describe Api::TransactionsController, type: :controller do
  let(:encoded_token) { token_generator(user.id)}
  let(:user) { create(:user) }
  let(:invalid_headers) { { 'Authorization' => nil } }

  describe 'GET /api/transactions' do
    context 'get list of transactions' do
      before  do
        user.token = encoded_token
        user.save
        gold = create(:asset)
        create(:cash)
        create(:buy, asset: gold, user: user)
        create(:top_up)
      end

      it "shows list of transactions" do
        request.headers['Authorization'] = user.token
        get :index, :format => :json
        expected_response = Transaction.all.map{|t| t.transaction_details_json }
        expect(json).to eq expected_response
        expect(response.status).to eq 200
      end
    end
  end
end
