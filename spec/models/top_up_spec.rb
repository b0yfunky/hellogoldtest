require 'rails_helper'

RSpec.describe TopUp, type: :model do
  subject { described_class.new}
  describe "Validations" do
    let(:user) { create(:user) }

    it "should have a type of TopUp" do
      expect(subject.type).to eq('TopUp')
    end

    it "should have a status pending and asset cash" do
      cash = create(:cash)
      subject.amount = 20
      subject.user_id = user.id
      subject.valid?
      subject.save
      expect(subject.asset).to eq(cash)
      expect(subject.status).to eq('pending')
    end
  end
end
