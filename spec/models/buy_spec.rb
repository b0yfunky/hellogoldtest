require 'rails_helper'

RSpec.describe Buy, type: :model do
  subject { described_class.new}
  describe "Validations" do

    it "should have a type of Buy" do
      expect(subject.type).to eq('Buy')
    end
  end
end
