require 'rails_helper'

RSpec.describe Balance, type: :model do
  subject { described_class.new}
  describe "Validations" do
    let(:user) { create(:user) }
    let(:asset) { create(:asset) }

    it "is valid with valid attributes" do
      subject.amount = 20
      subject.asset_id = asset.id
      subject.user_id = user.id
      expect(subject).to be_valid
    end

    it "is not valid without Asset, User and Amount" do
      expect(subject).to_not be_valid
    end
  end
end
