require 'rails_helper'

RSpec.describe Sell, type: :model do
  subject { described_class.new}
  describe "Validations" do

    it "should have a type of Sell" do
      expect(subject.type).to eq('Sell')
    end
  end
end
