FactoryBot.define do
  factory :asset do
    name { 'Gold' }
    slug { 'gold' }
    code { 'GLD' }

    factory :cash do
      name { 'Cash' }
      slug { 'cash' }
      code { 'CASH' }
    end
  end
end
