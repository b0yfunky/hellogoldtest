FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    country_code { 'my' }
    password { 'changeme!' }
    password_confirmation { 'changeme!' }
  end
end
