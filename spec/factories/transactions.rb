FactoryBot.define do
  factory :transaction do
    amount {20}
    asset_id {1}
    user_id {1}
    status { 'pending' }

    factory :buy do
      type { 'Buy' }
    end

    factory :top_up do
      type  { 'TopUp' }
    end
  end
end
