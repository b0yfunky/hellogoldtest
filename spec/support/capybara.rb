require "capybara-screenshot/rspec"
require "capybara/poltergeist"

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, window_size: [1200, 768], js_errors: false, timeout:10000, phantomjs:Phantomjs.path)
end

Capybara.javascript_driver = :poltergeist
Capybara.default_driver = Capybara.javascript_driver
Capybara.always_include_port = true
