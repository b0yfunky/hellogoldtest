module Formats::User
  def auth_details(options = {})
    {
      token: self.token,
      email: self.email
    }
  end
end
