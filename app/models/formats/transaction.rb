module Formats::Transaction
  def transaction_details_json(options = {})
    {
      amount: self.amount,
      asset: self.asset.slug,
      txnref: self.txnref,
      type: self.type,
      user_id: self.user_id
    }
  end
end
