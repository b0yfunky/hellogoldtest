module Formats::Balance
  def balance_details_json(options = {})
    {
      amount: self.amount,
      asset: self.asset.slug
    }
  end
end
