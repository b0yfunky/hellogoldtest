# frozen_string_literal: true

class User < ApplicationRecord
  include Formats::User
  has_many :balances
  has_many :assets, through: :balances

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable


  def can_trade_asset(asset, amount)
    asset_balance = self.balances.asset_balance(Asset.find_by_slug(asset).try(:id)).first
    return (asset_balance&.amount.to_f >= amount.to_f)
  end
end
