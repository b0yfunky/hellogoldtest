class Transaction < ApplicationRecord
  include Formats::Transaction
  belongs_to :user
  belongs_to :asset
  before_create :save_transaction_ref
  validates :amount, presence: true, numericality: {only_float: true}

  def save_transaction_ref
    self.txnref = 'TXN-' + DateTime.now.strftime("%Y%m%d%M%H%S")
  end
end
