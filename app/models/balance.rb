class Balance < ApplicationRecord
  include Formats::Balance
  belongs_to :user
  belongs_to :asset
  scope :asset_balance, -> (asset_id) { joins(:asset).where(asset_id: asset_id)}

end
