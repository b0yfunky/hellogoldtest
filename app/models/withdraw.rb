class Withdraw < Transaction
  before_validation :save_status, only: [:create]
  after_commit :complete_transaction

  def save_status
    self.asset_id = Asset.find_by_slug('cash').try(:id)
    self.status = "pending"
  end

  def complete_transaction
    if self.status == 'approved'
      asset_balance = self.user.balances.asset_balance(self.asset_id).first

      if asset_balance.present?
        new_amount = asset_balance.amount.to_f - self.amount.to_f
        asset_balance.update(amount: new_amount)
      end
    end
  end
end
