class Buy < Transaction

  def complete_transaction
    cash_balance = self.user.balances.asset_balance(Asset.find_by_slug('cash').try(:id)).first
    new_amount = cash_balance.amount.to_f - self.amount.to_f
    cash_balance.update(amount: new_amount)

    bought_asset_balance = self.user.balances.asset_balance(self.asset_id).first
    asset_amount = (self.amount / Asset::GOLD_ASSET).to_f
    if bought_asset_balance.present?
      new_asset_amount = bought_asset_balance.amount.to_f + asset_amount.to_f
      bought_asset_balance.update!(amount: new_asset_amount)
    else
      Balance.create!(amount: asset_amount, asset_id: asset_id, user: self.user)
    end
  end
end
