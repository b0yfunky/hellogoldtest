class Api::SessionsController <  Devise::SessionsController
  skip_before_action :verify_authenticity_token
  def create
    self.resource = warden.authenticate!(scope: :user, store: is_navigational_format?)
    respond_to do |format|
      format.json {
        current_user.update_attribute(:token, ::JsonWebToken.encode(user_id: current_user.id))
        sign_in current_user, store: false
        render json: {user: current_user.auth_details}, status: 200
      }
      format.any {
        respond_with current_user, location: after_sign_in_path_for(current_user)
      }
    end
  end
end
