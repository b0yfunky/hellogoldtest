class Api::BalancesController < Api::ApplicationController
  def index
    render json: @user.balances.map{|t| t.balance_details_json }
  end
end
