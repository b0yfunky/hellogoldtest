class Api::ApplicationController < ActionController::API
  rescue_from ::ActiveRecord::RecordNotFound, with: :not_found
  rescue_from ::NameError, with: :internal_server_error
  rescue_from ::ActionController::RoutingError, with: :internal_server_error

  include ApiAuthenticator

  # before_action :authenticate_api_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password])
  end

  [:not_found, :internal_server_error].each do |method|
    define_method method do |exception|
      render json: {error: exception.message}.to_json, status: Rack::Utils::SYMBOL_TO_STATUS_CODE[method]
      return
    end
  end
end
