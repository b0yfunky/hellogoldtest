class Api::TransactionsController < Api::ApplicationController
  before_action :set_asset_and_user, only: [:buy, :sell, :top_up, :withdraw]

  def index
    render json: Transaction.all.map{|t| t.transaction_details_json }
  end

  def buy
    if @user.can_trade_asset('cash', params[:amount])
      buy_transaction = Buy.new(transactions_params)
      Buy.transaction do
        if buy_transaction.save
          buy_transaction.complete_transaction
          render json: {result: "OK"}
        else
          render status: 422, json: {error: 'Failed to complete transaction'}
        end
      end
    else
      render status: 422, json: {error: 'No enough cash to perform transaction'}
    end
  end

  def sell
    if @user.can_trade_asset(params[:asset], params[:amount])
      sell_transaction = Sell.new(transactions_params)
      Sell.transaction do
        if sell_transaction.save
          sell_transaction.complete_transaction
          render json: {result: "OK"}
        else
          render status: 422, json: {error: 'Failed to complete transaction'}
        end
      end
    else
      render status: 422, json: {error: 'No enough asset to perform transaction'}
    end
  end

  def top_up
    top_up_transaction = TopUp.new(transactions_params)
    TopUp.transaction do
      if top_up_transaction.save
        render json: {result: "OK"}
      else
        render status: 422, json: {error: 'Failed to complete transaction'}
      end
    end
  end

  def withdraw
    if @user.can_trade_asset('cash', params[:amount])
      withdraw_transaction = Withdraw.new(transactions_params)
      Withdraw.transaction do
        if withdraw_transaction.save!
          render json: {result: "OK"}
        else
          render status: 422, json: {error: 'Failed to complete transaction'}
        end
      end
    else
      render status: 422, json: {error: 'No enough asset to perform transaction'}
    end
  end

  private

  def set_asset_and_user
    byebug
    params[:transaction][:asset_id] = Asset.find_by_slug(params[:asset]).try(:id) if params[:asset].present?
    params[:transaction][:user_id] = @user.try(:id) if @user.present?
  end

  def transactions_params
    params.require(:transaction).permit(:amount, :asset_id, :user_id)
  end

end
