class Api::RegistrationsController < Devise::RegistrationsController
  skip_before_action :verify_authenticity_token
  prepend_before_action :require_no_authentication, only: [:create]
  before_action :configure_permitted_parameters, if: :devise_controller?
  wrap_parameters User, include: [:name, :email, :country_code, :password, :password_confirmation]

  def create
    location_success = false
    build_resource(sign_up_params)

    resource.save

    if resource.persisted?
      if resource.active_for_authentication?
        sign_up(resource_name, resource)
        resource.update_attribute(:token, ::JsonWebToken.encode(user_id: resource.id))
        location_success = true
      else
        expire_data_after_sign_in!
        location_success = false
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
    end
    respond_to do |format|
      format.json {
        resource.errors.present? ? render_create_error : render_create_success
      }
      format.any {
        respond_with resource, location: location_success == true ? after_sign_up_path_for(resource) : after_inactive_sign_up_path_for(resource)
      }
    end
  end

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :country_code, :password, :password_confirmation)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:country_code])
  end

  def render_create_error
    render json: {
      status: 'error',
      errors: resource.errors.messages
    }, status: 422
  end

  def render_create_success
    render json: {
      result: 'success',
      user:   resource.auth_details
    }
  end

end
