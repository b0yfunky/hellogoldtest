class Admins::TransactionsController < AdminsController
  before_action :get_transaction_id, only: [:update]

  def index
    @transactions = Transaction.where(status: 'pending')
  end

  def update
    @transaction.update_attributes(transactions_params)
    redirect_to admins_transactions_path
  end

  private

  def get_transaction_id
    @transaction = Transaction.find_or_initialize_by(id: params[:id])
  end

  def transactions_params
    params.require(:transaction).permit(:amount, :asset_id, :user_id, :status, :txnref)
  end
end
