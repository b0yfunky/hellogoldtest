class CreateTransaction < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.float :amount
      t.string :txnref
      t.string :status
      t.integer :asset_id
      t.integer :user_id
      t.datetime :deleted_at
      t.index :deleted_at
      t.string :type
      t.timestamps null: false
    end
  end
end
