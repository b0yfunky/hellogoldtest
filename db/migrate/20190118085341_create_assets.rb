class CreateAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :assets do |t|
      t.string :name
      t.string :slug
      t.string :code
      t.datetime :deleted_at
      t.index :deleted_at
      t.timestamps null: false
    end
  end
end
