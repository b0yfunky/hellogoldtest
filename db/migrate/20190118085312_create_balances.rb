class CreateBalances < ActiveRecord::Migration[5.1]
  def change
    create_table :balances do |t|
      t.float :amount
      t.integer :asset_id
      t.integer :user_id
      t.datetime :deleted_at
      t.index :deleted_at
      t.timestamps null: false
    end
  end
end
